MODULE := dynamojson

.PHONY: debug
debug: $(eval TGT:=debug)
debug: wasm

.PHONY: release
release: $(eval TGT:=release)
release: RELFLAGS = --release
release: wasm

.PHONY: wasm
wasm:
	cargo wasi build --lib $(RELFLAGS)

.PHONY: test
test: debug
	writ \
		--wit $(MODULE).wit --expect '{"id":"05b8c86b-9591-49c1-939c-4657ee522809","payments":["05b8c86b-9591-49c1-939c-4657ee522809"]}' target/wasm32-wasi/debug/$(MODULE).wasm deserialize-dynamo-json \
		{"Item":{"id":{"S":"05b8c86b-9591-49c1-939c-4657ee522809"},"payments":{"L":[{"S":"05b8c86b-9591-49c1-939c-4657ee522809"}]}}}
	@echo PASS

.PHONY: clean
clean:
	@cargo clean