wit_bindgen_rust::export!("dynamojson.wit");
struct Dynamojson;

use serde_json::{Value, Map};

fn convert_dynamo_json(value: &Value) -> Value {
    match value {
        Value::Object(map) => {
            let mut result = Map::new();
            for (key, val) in map {
                result.insert(key.clone(), convert_single_attribute(val));
            }
            Value::Object(result)
        }
        _ => convert_single_attribute(value),
    }
}

fn convert_single_attribute(value: &Value) -> Value {
    match value {
        Value::Object(map) => {
            if let Some((type_key, type_value)) = map.iter().next() {
                match type_key.as_str() {
                    "S" | "N" => type_value.clone(),
                    "BOOL" => Value::Bool(type_value.as_bool().unwrap_or(false)),
                    "NULL" => Value::Null,
                    "L" => Value::Array(
                        type_value
                            .as_array()
                            .unwrap_or(&vec![])
                            .iter()
                            .map(convert_single_attribute)
                            .collect(),
                    ),
                    "M" => convert_dynamo_json(type_value),
                    _ => convert_dynamo_json(value),
                }
            } else {
                convert_dynamo_json(value)
            }
        }
        _ => value.clone(),
    }
}

impl dynamojson::Dynamojson for Dynamojson {
    fn deserialize_dynamo_json(json: String) -> String {
        // Replace single quotes with double quotes
        let processed_json = json.replace('\'', "`");

        // Parse the input JSON string
        let parsed: Value = match serde_json::from_str(&processed_json) {
            Ok(v) => v,
            Err(e) => {
                eprintln!("Error parsing JSON: {}", e);
                return String::from("{}");
            }
        };
        
        // Convert the entire JSON structure
        let converted = convert_dynamo_json(&parsed);

        // Serialize the converted JSON to a string
        serde_json::to_string(&converted).unwrap_or_else(|e| {
            eprintln!("Error serializing to JSON: {}", e);
            String::from("{}")
        })
    }
}
