# SingleStore Extension: Serialize Dynamo DB JSON Object

The Serialize Dynamo DB JSON Object - SingleStore DB extension, written in Rust.

## Building

In order to build the Wasm

1. Install Rust and Cargo
2. Install the Rust `wasm32-wasi` target
3. Run `cargo wasi build --lib --release`

To build the extension from the Wasm locally, run the following.

```bash
cp target/wasm32-wasi/release/dynamojson.wasm ./
tar cvf dynamojson.tar dynamojson.sql dynamojson.wasm dynamojson.wit 
```

## Deploying

```sql
CREATE EXTENSION dynamojson FROM HTTP 'https://gitlab.com/menglert/s2.dynamojson-extension/-/raw/main/dynamojson.tar';
```
* Verify the Extension `SHOW EXTENSIONS;`
* Verify the Functions `SHOW FUNCTIONS;`

## Running

```sql
SET @newImage = '{"Item": {"id": {"N": "9"},"total_cost": {"N": "46.42"},"transaction_at": {"N": "1720421227"}}}':>JSON;

SELECT deserialize_dynamo_json(@newImage);
```
`{"Item":{"id":"9","total_cost":"46.42","transaction_at":"1720421227"}}`

```sql
SET @newImage = '{"Item": {"id": {"N": "9"},"total_cost": {"N": "46.42"},"transaction_at": {"N": "1720421227"}}}':>JSON;

SELECT deserialize_dynamo_json(@newImage::`Item`);
```
`{"id":"9","total_cost":"46.42","transaction_at":"1720421227"}`